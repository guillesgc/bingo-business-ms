package com.progracol.bingo.http.client.api;

import com.progracol.bingo.common.constant.Header;
import com.progracol.bingo.common.response.UserResponse;
import com.progracol.bingo.common.route.ExternalRoute;
import com.progracol.bingo.http.client.configuration.FeignConfiguration;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;

@FeignClient(
        name = ExternalRoute.BINGO_CLIENT_NAME,
        url = "${microservices.bingo.api.client.base_url}",
        configuration = {FeignConfiguration.class}
)
public interface BingoApiClient {

    @GetMapping(ExternalRoute.GET_USER_FROM_TOKEN)
    UserResponse getUserFromToken(@RequestHeader(Header.AUTHORIZATION) String accessToken);
}

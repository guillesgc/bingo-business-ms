package com.progracol.bingo.http.client.configuration;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.progracol.bingo.common.exception.BingoException;
import com.progracol.bingo.common.response.ErrorResponse;
import feign.Response;
import feign.codec.ErrorDecoder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

import static org.apache.http.HttpStatus.SC_BAD_REQUEST;
import static org.apache.http.HttpStatus.SC_CONFLICT;
import static org.apache.http.HttpStatus.SC_INTERNAL_SERVER_ERROR;
import static org.apache.http.HttpStatus.SC_GATEWAY_TIMEOUT;
import static org.apache.http.HttpStatus.SC_UNAUTHORIZED;
import static org.apache.http.HttpStatus.SC_FAILED_DEPENDENCY;

public class FeignErrorDecoder implements ErrorDecoder {

    private Logger logger = LoggerFactory.getLogger(FeignErrorDecoder.class);

    /**
     * Decode Feign Response
     * @param methodKey
     * @param response
     * @return
     */
    @Override
    public Exception decode(String methodKey, Response response) {
        switch (response.status()) {
            case SC_BAD_REQUEST:
            case SC_CONFLICT:
            case SC_INTERNAL_SERVER_ERROR:
            case SC_GATEWAY_TIMEOUT:
            case SC_UNAUTHORIZED:
            case SC_FAILED_DEPENDENCY:
                try {
                    String bodyResponse = new String(feign.Util.toByteArray(response.body().asInputStream()), feign.Util.UTF_8);
                    logger.error("FeignErrorDecoder::decode --bodyResponse [{}}", bodyResponse);
                    BingoException exception = getErrorResponse(response, bodyResponse);
                    if(exception == null) {
                        exception = new BingoException(response.status(), bodyResponse, response.reason());
                    }
                    throw exception;
                } catch (IOException e) {
                    logger.error("FeignErrorDecoder::decode --ERROR [{}}", e.getMessage());
                    throw new BingoException(response.status(),
                            response.reason(),
                            "Unexpected Exception");
                }
            default:
                throw new IllegalStateException("Unexpected value: " + response.status());
        }
    }

    /**
     * Get BingoException from Response
     * @param response
     * @param bodyResponse
     * @return
     */
    private BingoException getErrorResponse(Response response, String bodyResponse) {
        try {
            ObjectMapper responseData = new ObjectMapper();
            ErrorResponse errorResponse = responseData.readValue(bodyResponse, ErrorResponse.class);
            return new BingoException(response.status(), errorResponse.getError().getCode(), errorResponse.getError().getMessage());
        } catch (IOException e) {
            logger.error("FeignErrorDecoder::decode --ERROR [{}}", e.getMessage());
            return  null;
        }
    }
}

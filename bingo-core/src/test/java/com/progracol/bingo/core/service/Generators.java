package com.progracol.bingo.core.service;

import com.progracol.bingo.common.request.EvaluateBoardRequest;
import com.progracol.bingo.common.request.UpdateFigureRequest;
import com.progracol.bingo.common.response.BoardResponse;
import com.progracol.bingo.common.response.PageBoardResponse;
import com.progracol.bingo.common.response.UserResponse;
import com.progracol.bingo.domain.entity.BoardEntity;
import com.progracol.bingo.domain.entity.FigureEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.ArrayList;
import java.util.List;

public final class Generators {
    public static PageBoardResponse getPageBoardResponse(){
        List<BoardEntity> boards = new ArrayList<>();
        BoardEntity board = new BoardEntity();
        boards.add(board);
        return new PageBoardResponse(boards, 1,  1, 1);
    }

    public static BoardResponse getBoardResponse() {
        List<Integer> boardNumbers = new ArrayList(){{ add(12); add(32); add(43); }};
        return new BoardResponse(1L,boardNumbers);
    }

    public static UserResponse getUserResponse(){
        UserResponse userResponse = new UserResponse();
        userResponse.setId("1");
        userResponse.setUsername("bingo");
        return userResponse;
    }

    public static List<FigureEntity> getFigureEntityList() {
        List<FigureEntity> getAllFigure = new ArrayList();
        getAllFigure.add(new FigureEntity());
        getAllFigure.add(new FigureEntity());
        return getAllFigure;
    }

    public static FigureEntity getFigureEntity(){
        FigureEntity figureEntity = new FigureEntity();
        figureEntity.setFigureId(1L);
        figureEntity.setFigureName("N");
        figureEntity.setPositionsWinner(new ArrayList<Boolean>() {{ add(true); add(false); add(true); }});
        return figureEntity;
    }

    public static UpdateFigureRequest getUpdateFigureRequest() {
        UpdateFigureRequest figureRequest = new UpdateFigureRequest();
        figureRequest.setFigureId(1L);
        figureRequest.setFigureName("N");
        figureRequest.setPositionsWinner(new ArrayList<Boolean>() {{ add(true); add(false); add(true); }});
        return figureRequest;
    }

    public static EvaluateBoardRequest getEvaluateBoardRequest() {
        EvaluateBoardRequest evaluateBoardRequest = new EvaluateBoardRequest();
        evaluateBoardRequest.setWinningNumbers(new ArrayList(){{ add(12); add(32); add(43); }});
        return evaluateBoardRequest;
    }

    public static BoardEntity getBoardEntity() {
        BoardEntity boardEntity = new BoardEntity();
        boardEntity.setBoardId(1L);
        boardEntity.setBoardNumbers(new ArrayList(){{ add(12); add(32); add(43); }});
        return boardEntity;
    }

    public static Page<BoardEntity> getPageBoardEntity() {
        List<BoardEntity> content = new ArrayList() {{add(getBoardEntity());}};
        Pageable pageable = PageRequest.of(1, 10);
        Page<BoardEntity> page = new PageImpl(content, pageable, 10);
        return page;
    }

    public static List<BoardEntity> getBoardEntityList() {
        return new ArrayList(){{ add(getBoardEntity());}};
    }
}

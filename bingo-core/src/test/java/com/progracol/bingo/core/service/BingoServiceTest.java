package com.progracol.bingo.core.service;

import com.progracol.bingo.app.Application;
import com.progracol.bingo.common.request.EvaluateBoardRequest;
import com.progracol.bingo.common.request.UpdateFigureRequest;
import com.progracol.bingo.common.response.BoardResponse;
import com.progracol.bingo.common.response.PageBoardResponse;
import com.progracol.bingo.common.response.UserResponse;
import com.progracol.bingo.domain.entity.BoardEntity;
import com.progracol.bingo.domain.entity.FigureEntity;
import com.progracol.bingo.domain.repository.BoardRepository;
import com.progracol.bingo.domain.repository.FigureRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@SpringBootTest(classes = Application.class)
@ExtendWith(SpringExtension.class)
public class BingoServiceTest {

    @InjectMocks
    private BingoService bingoService;

    @Mock
    private MessageSource messageSource;

    @Mock
    private BoardRepository boardRepository;

    @Mock
    private FigureRepository figureRepository;

    @Test
    public void getBoardTest(){
        int page = 1;
        int size = 10;
        Page<BoardEntity> pages = Generators.getPageBoardEntity();
        Mockito.doReturn(pages).when(boardRepository).findAll(pages.getPageable());

        PageBoardResponse pageBoardResponse = bingoService.getBoard(page, size);
        Assertions.assertNotNull(pageBoardResponse);
        Assertions.assertEquals(2, pageBoardResponse.getTotalPages());
        Assertions.assertNotNull(pageBoardResponse.getBoards());
    }

    @Test
    public void getBoardByIdTest(){
        Long boardId = 1L;
        boardRepository.findById(boardId);
        Optional<BoardEntity> boardsOptional = Optional.of(Generators.getBoardEntity());
        Mockito.doReturn(boardsOptional).when(boardRepository).findById(boardId);

        BoardResponse boardResponse = bingoService.getBoardById(boardId);
        Assertions.assertNotNull(boardResponse);
        Assertions.assertEquals(1L, boardResponse.getBoardId());
        Assertions.assertNotNull(boardResponse.getBoardNumbers());
    }

    @Test
    public void evaluateBoardTest(){
        EvaluateBoardRequest evaluateBoardRequest = Generators.getEvaluateBoardRequest();
        List<BoardEntity> boardEntities = Generators.getBoardEntityList();
        Mockito.doReturn(boardEntities).when(boardRepository).findAll();

        Stream<BoardEntity> boardResponse = bingoService.evaluateBoard(evaluateBoardRequest);
        Assertions.assertNotNull(boardResponse);
        Assertions.assertEquals(1L, boardResponse.collect(Collectors.toList()).size());
    }

    @Test
    public void etAllFigureTest(){
        List<FigureEntity> figures = Generators.getFigureEntityList();
        Mockito.doReturn(figures).when(figureRepository).findAll();

        List<FigureEntity> result = bingoService.getAllFigure();
        Assertions.assertNotNull(result);
        Assertions.assertEquals(figures.size(), result.size());
    }

    @Test
    public void updatePositionWinnerFigureTest(){
        FigureEntity figure = Generators.getFigureEntity();
        UpdateFigureRequest figureRequest = Generators.getUpdateFigureRequest();
        UserResponse user = Generators.getUserResponse();
        Mockito.doReturn(Optional.of(figure)).when(figureRepository).findById(figure.getFigureId());
        Mockito.doReturn(figure).when(figureRepository).save(Mockito.any());

        FigureEntity result = bingoService.updatePositionWinnerFigure(figureRequest, user);
        Assertions.assertNotNull(result);
        Assertions.assertEquals(figure.getFigureId(), result.getFigureId());
    }
}

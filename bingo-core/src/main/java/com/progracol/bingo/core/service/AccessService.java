package com.progracol.bingo.core.service;

import com.progracol.bingo.common.response.UserResponse;
import com.progracol.bingo.http.client.api.BingoApiClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AccessService {

    private Logger logger = LoggerFactory.getLogger(AccessService.class);

    @Autowired
    private BingoApiClient bingoApiClient;

    public UserResponse getUserFromToken(String accessToken) {
        return bingoApiClient.getUserFromToken(accessToken);
    }
}

package com.progracol.bingo.core.service;

import com.progracol.bingo.common.exception.BingoException;
import com.progracol.bingo.common.mapper.BoardMapper;
import com.progracol.bingo.common.request.EvaluateBoardRequest;
import com.progracol.bingo.common.request.UpdateFigureRequest;
import com.progracol.bingo.common.response.BoardResponse;
import com.progracol.bingo.common.response.PageBoardResponse;
import com.progracol.bingo.common.response.UserResponse;
import com.progracol.bingo.domain.entity.BoardEntity;
import com.progracol.bingo.domain.entity.FigureEntity;
import com.progracol.bingo.domain.repository.BoardRepository;
import com.progracol.bingo.domain.repository.FigureRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import static com.progracol.bingo.common.constant.ErrorCode.ITEM_NOT_FOUND;

@Service
public class BingoService {

    private static final String CLASS = BingoService.class.getSimpleName();
    private Logger logger = LoggerFactory.getLogger(BingoService.class);

    @Autowired
    private MessageSource messageSource;

    @Autowired
    private BoardRepository boardRepository;

    @Autowired
    private FigureRepository figureRepository;

    /**
     * This method allows getting boards using paging.
     * @param page
     * @param size
     * @return @see PageBoardResponse
     */
    public PageBoardResponse getBoard(int page, int size){
        logger.info("--BINGO-MS --$CLASS[{}]:getBoard --page[{}] --size[{}]",  CLASS, page, size);
        Pageable paging = PageRequest.of(page, size);
        Page<BoardEntity> pageBoards = boardRepository.findAll(paging);

        PageBoardResponse pageBoardResponse = new PageBoardResponse(
                pageBoards.getContent(),
                pageBoards.getTotalElements(),
                pageBoards.getTotalPages(),
                pageBoards.getNumber()
                );

        return pageBoardResponse;
    }

    /**
     * This method allow to find a board by Id
     * @param boardId
     * @return BoardResponse
     */
    public BoardResponse getBoardById(Long boardId){
        logger.info("--BINGO-MS --$CLASS[{}]:getBoardById --boardId[{}]", CLASS, boardId);
        BoardResponse response = null;
        Optional<BoardEntity> result = boardRepository.findById(boardId);

        if(result.isPresent()) {
            response = BoardMapper.BoardEntityToBoardResponse(result.get());
        }else {
            String message = messageSource.getMessage(ITEM_NOT_FOUND, null, LocaleContextHolder.getLocale());
            throw new BingoException(HttpStatus.BAD_REQUEST.value(), ITEM_NOT_FOUND, message);
        }
        return response;
    }

    /**
     * This method is using to evaluating which boards were the winner.
     * @param evaluateBoardRequest
     * @return List of boards winner
     */
    public Stream<BoardEntity> evaluateBoard(EvaluateBoardRequest evaluateBoardRequest){
        return boardRepository.findAll().parallelStream()
            .filter(item -> item.getBoardNumbers().containsAll(evaluateBoardRequest.getWinningNumbers()))
            .limit(100);
    }

    /**
     * Allow to get all figure
     * @return list of figures
     */
    public List<FigureEntity> getAllFigure(){
        return figureRepository.findAll();
    }

    /**
     * Allow updating the positions of a figure entity.
     * @param figureRequest
     * @param user
     * @return Figure updated
     */
    public FigureEntity updatePositionWinnerFigure(UpdateFigureRequest figureRequest, UserResponse user){
        logger.info("--BINGO-MS --$CLASS[{}]:updatePositionWinnerFigure --FigureId[{}]",  CLASS, figureRequest.getFigureId());
        FigureEntity response = null;
        Optional<FigureEntity> element = figureRepository.findById(figureRequest.getFigureId());
        if(element.isPresent()) {
            FigureEntity found = element.get();
            found.setPositionsWinner(figureRequest.getPositionsWinner());
            found.setLastUpdatedAt(LocalDateTime.now());
            found.setLastUpdatedBy(Integer.parseInt(user.getId()));
            response = figureRepository.save(element.get());
        }
        return response;
    }
}

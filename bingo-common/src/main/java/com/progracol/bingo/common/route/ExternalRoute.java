package com.progracol.bingo.common.route;

public interface ExternalRoute {
    String BINGO_CLIENT_NAME = "bingo-api-client";
    String USER = "/user";
    String GET_USER_FROM_TOKEN = USER+"/from-token";
}

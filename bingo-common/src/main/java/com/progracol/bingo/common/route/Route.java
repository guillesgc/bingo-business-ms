package com.progracol.bingo.common.route;

public interface Route {
    String HEALTH = "/health";
    String GAME = "/game";
    String BOARD = "/board";
    String GET_BOARD_BY_ID = BOARD + "/{id}";
    String EVALUATE = "/evaluate";
    String FIGURE = "/figure";
}

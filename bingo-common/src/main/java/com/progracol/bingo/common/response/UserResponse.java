package com.progracol.bingo.common.response;

public class UserResponse {

    private String id;

    private String names;

    private String username;

    private String password;

    public UserResponse() {

    }
    public UserResponse(String id, String names, String username, String password) {
        this.id = id;
        this.names = names;
        this.username = username;
        this.password = password;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNames() {
        return names;
    }

    public void setNames(String names) {
        this.names = names;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}

package com.progracol.bingo.common.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UpdateFigureRequest {

    @NotNull
    private Long figureId;

    private Integer groupFigureId;

    private String figureName;

    @NotNull @NotEmpty
    private List<Boolean> positionsWinner;

    private String status;

    private Boolean used;

    public Long getFigureId() {
        return figureId;
    }

    public void setFigureId(Long figureId) {
        this.figureId = figureId;
    }

    public Integer getGroupFigureId() {
        return groupFigureId;
    }

    public void setGroupFigureId(Integer groupFigureId) {
        this.groupFigureId = groupFigureId;
    }

    public String getFigureName() {
        return figureName;
    }

    public void setFigureName(String figureName) {
        this.figureName = figureName;
    }

    public List<Boolean> getPositionsWinner() {
        return positionsWinner;
    }

    public void setPositionsWinner(List<Boolean> positionsWinner) {
        this.positionsWinner = positionsWinner;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Boolean getUsed() {
        return used;
    }

    public void setUsed(Boolean used) {
        this.used = used;
    }
}

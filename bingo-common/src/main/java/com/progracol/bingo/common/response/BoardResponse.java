package com.progracol.bingo.common.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import java.util.List;

@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class BoardResponse {

    private Long boardId;

    private List<Integer> boardNumbers;

    public BoardResponse(Long boardId, List<Integer> boardNumbers) {
        this.boardId = boardId;
        this.boardNumbers = boardNumbers;
    }

    public Long getBoardId() {
        return boardId;
    }

    public void setBoardId(Long boardId) {
        this.boardId = boardId;
    }

    public List<Integer> getBoardNumbers() {
        return boardNumbers;
    }

    public void setBoardNumbers(List<Integer> boardNumbers) {
        this.boardNumbers = boardNumbers;
    }

}

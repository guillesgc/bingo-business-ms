package com.progracol.bingo.common.mapper;

import com.progracol.bingo.common.response.BoardResponse;
import com.progracol.bingo.domain.entity.BoardEntity;

public class BoardMapper {

    /**
     * Mapper a BoardEntity to BoardResponse
     * @param entity
     * @return BoardResponse
     */
    public static BoardResponse BoardEntityToBoardResponse(BoardEntity entity){
        BoardResponse response = new BoardResponse(entity.getBoardId(), entity.getBoardNumbers());
        return response;
    }
}

package com.progracol.bingo.domain.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.vladmihalcea.hibernate.type.array.ListArrayType;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.List;

@Entity
@Table(name = "bingo_param_board")
@TypeDef(
        name = "list-array",
        typeClass = ListArrayType.class
)
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class BoardEntity {

    @Id
    @Column
    private Long boardId;

    @Type(type = "list-array")
    @Column(
            name = "board_numbers",
            columnDefinition = "integer[]"
    )
    private List<Integer> boardNumbers;

    public Long getBoardId() {
        return boardId;
    }

    public void setBoardId(Long boardId) {
        this.boardId = boardId;
    }

    public List<Integer> getBoardNumbers() {
        return boardNumbers;
    }

    public void setBoardNumbers(List<Integer> boardNumbers) {
        this.boardNumbers = boardNumbers;
    }

}

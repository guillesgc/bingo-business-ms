package com.progracol.bingo.domain.repository;

import com.progracol.bingo.domain.entity.FigureEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FigureRepository extends JpaRepository<FigureEntity, Long> {

}

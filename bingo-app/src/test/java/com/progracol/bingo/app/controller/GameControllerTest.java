package com.progracol.bingo.app.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.progracol.bingo.common.request.EvaluateBoardRequest;
import com.progracol.bingo.common.request.UpdateFigureRequest;
import com.progracol.bingo.common.response.BoardResponse;
import com.progracol.bingo.common.response.PageBoardResponse;
import com.progracol.bingo.common.route.Route;
import com.progracol.bingo.core.service.BingoService;
import com.progracol.bingo.domain.entity.BoardEntity;
import com.progracol.bingo.domain.entity.FigureEntity;
import com.progracol.bingo.domain.repository.FigureRepository;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.OverrideAutoConfiguration;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.List;
import java.util.stream.Stream;

@WebMvcTest(controllers = GameController.class)
@OverrideAutoConfiguration(enabled = true)
public class GameControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private BingoService bingoService;

    @Mock
    private FigureRepository figureRepository;

    @Test
    public void getBoardTest() throws Exception {

        int page = 0;
        int size = 10;
        ObjectMapper objectMapper = new ObjectMapper();
        PageBoardResponse pageBoardResponse = Generators.getPageBoardResponse();
        String jsonResponse = objectMapper.writeValueAsString(pageBoardResponse);

        String url = Route.GAME.concat(Route.BOARD);

        Mockito.doReturn(pageBoardResponse).when(bingoService).getBoard(page, size);

        mvc.perform(
                MockMvcRequestBuilders.get(url)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
        ).andExpect(MockMvcResultMatchers.status().isOk())

                .andExpect(
                        MockMvcResultMatchers.content().string(Matchers.containsString(jsonResponse))
                )
                .andDo(MockMvcResultHandlers.print())
                .andReturn();
    }

    @Test
    public void getBoardByIdTest() throws Exception {

        Long boardId = 1L;
        ObjectMapper objectMapper = new ObjectMapper();
        BoardResponse boardResponse = Generators.getBoardResponse();
        String jsonResponse = objectMapper.writeValueAsString(boardResponse);

        String url = Route.GAME.concat(Route.GET_BOARD_BY_ID).replace("{id}", boardId.toString());

        Mockito.doReturn(boardResponse).when(bingoService).getBoardById(boardId);

        mvc.perform(
                MockMvcRequestBuilders.get(url)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
        ).andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(
                        MockMvcResultMatchers.content().string(Matchers.containsString(jsonResponse))
                )
                .andDo(MockMvcResultHandlers.print())
                .andReturn();
    }

    @Test
    public void getFigureEntityList() throws Exception {

        ObjectMapper objectMapper = new ObjectMapper();
        List<FigureEntity> figures = Generators.getFigureEntityList();
        String jsonResponse = objectMapper.writeValueAsString(figures);

        String url = Route.GAME.concat(Route.FIGURE);

        Mockito.doReturn(figures).when(bingoService).getAllFigure();

        mvc.perform(
                MockMvcRequestBuilders.get(url)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
        ).andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(
                        MockMvcResultMatchers.content().json(jsonResponse)
                )
                .andDo(MockMvcResultHandlers.print())
                .andReturn();
    }

    @Test
    public void updatePositionWinnerFigure() throws Exception {

        ObjectMapper objectMapper = new ObjectMapper();
        UpdateFigureRequest updateFigureRequest = Generators.getUpdateFigureRequest();
        String jsonRequest = objectMapper.writeValueAsString(updateFigureRequest);
        FigureEntity figureEntity = Generators.getFigureEntity();
        String jsonResponse = objectMapper.writeValueAsString(figureEntity);

        String url = Route.GAME.concat(Route.FIGURE);

        Mockito.doReturn(figureEntity).when(bingoService).updatePositionWinnerFigure(Mockito.any(), Mockito.any());

        mvc.perform(
                MockMvcRequestBuilders.put(url)
                        .content(jsonRequest)
                        .session(buildMockSession())
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
        ).andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(
                        MockMvcResultMatchers.content().json(jsonResponse)
                )
                .andDo(MockMvcResultHandlers.print())
                .andReturn();
    }

    @Test
    public void evaluateBoardTest() throws Exception {

        ObjectMapper objectMapper = new ObjectMapper();
        EvaluateBoardRequest evaluateBoardRequest = Generators.getEvaluateBoardRequest();
        String jsonRequest = objectMapper.writeValueAsString(evaluateBoardRequest);
        Stream<BoardEntity> result = Stream.of(Generators.getBoardEntity());

        String url = Route.GAME.concat(Route.EVALUATE);

        Mockito.doReturn(result).when(bingoService).evaluateBoard(Mockito.any());

        mvc.perform(
                MockMvcRequestBuilders.post(url)
                        .content(jsonRequest)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
        ).andExpect(MockMvcResultMatchers.status().isOk())
                .andDo(MockMvcResultHandlers.print())
                .andReturn();
    }

    private MockHttpSession buildMockSession() {
        MockHttpSession mockSession = new MockHttpSession(null, "user");
        mockSession.setAttribute("user", Generators.getUserResponse());
        return mockSession;
    }

}

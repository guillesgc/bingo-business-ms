package com.progracol.bingo.app.controller;

import com.progracol.bingo.common.request.EvaluateBoardRequest;
import com.progracol.bingo.common.request.UpdateFigureRequest;
import com.progracol.bingo.common.response.BoardResponse;
import com.progracol.bingo.common.response.PageBoardResponse;
import com.progracol.bingo.common.response.UserResponse;
import com.progracol.bingo.domain.entity.BoardEntity;
import com.progracol.bingo.domain.entity.FigureEntity;

import java.util.ArrayList;
import java.util.List;

public final class Generators {
    public static PageBoardResponse getPageBoardResponse(){
        List<BoardEntity> boards = new ArrayList<>();
        BoardEntity board = new BoardEntity();
        boards.add(board);
        return new PageBoardResponse(boards, 1,  1, 1);
    }

    public static BoardResponse getBoardResponse() {
        List<Integer> boardNumbers = new ArrayList(){{ add(12); add(32); add(43); }};
        return new BoardResponse(1L,boardNumbers);
    }

    public static UserResponse getUserResponse(){
        UserResponse userResponse = new UserResponse();
        userResponse.setId("1");
        userResponse.setUsername("bingo");
        return userResponse;
    }

    public static List<FigureEntity> getFigureEntityList() {
        List<FigureEntity> getAllFigure = new ArrayList();
        getAllFigure.add(new FigureEntity());
        getAllFigure.add(new FigureEntity());
        return getAllFigure;
    }
    public static FigureEntity getFigureEntity(){
        FigureEntity figureEntity = new FigureEntity();
        figureEntity.setFigureId(1L);
        figureEntity.setFigureName("N");
        figureEntity.setPositionsWinner(new ArrayList<Boolean>() {{ add(true); add(false); add(true); }});
        return figureEntity;
    }

    public static UpdateFigureRequest getUpdateFigureRequest() {
        UpdateFigureRequest figureRequest = new UpdateFigureRequest();
        figureRequest.setFigureId(1L);
        figureRequest.setFigureName("N");
        figureRequest.setPositionsWinner(new ArrayList<Boolean>() {{ add(true); add(false); add(true); }});
        return figureRequest;
    }

    public static EvaluateBoardRequest getEvaluateBoardRequest() {
        EvaluateBoardRequest evaluateBoardRequest = new EvaluateBoardRequest();
        evaluateBoardRequest.setWinningNumbers(new ArrayList(){{ add(12); add(32); add(43); }});
        return evaluateBoardRequest;
    }

    public static BoardEntity getBoardEntity() {
        BoardEntity boardEntity = new BoardEntity();
        boardEntity.setBoardNumbers(new ArrayList(){{ add(12); add(32); add(43); }});
        return boardEntity;
    }
}

package com.progracol.bingo.app.controller;

import com.progracol.bingo.common.request.EvaluateBoardRequest;
import com.progracol.bingo.common.request.UpdateFigureRequest;
import com.progracol.bingo.common.response.BoardResponse;
import com.progracol.bingo.common.response.PageBoardResponse;
import com.progracol.bingo.common.response.UserResponse;
import com.progracol.bingo.common.route.Route;
import com.progracol.bingo.core.service.BingoService;
import com.progracol.bingo.domain.entity.BoardEntity;
import com.progracol.bingo.domain.entity.FigureEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;
import java.util.stream.Stream;

@RestController
@RequestMapping(value = {(Route.GAME)}, produces = {(MediaType.APPLICATION_JSON_VALUE)})
public class GameController {

    @Autowired
    private BingoService bingoService;

    /**
     * Endpoint to getting boards using paging.
     * @param page
     * @param size
     * @return PageBoardResponse
     */
    @GetMapping(Route.BOARD)
    public PageBoardResponse getBoard(
            @RequestParam(defaultValue = "0") int page,
            @RequestParam(defaultValue = "10") int size) {
        return bingoService.getBoard(page, size);
    }

    /**
     * Endpoint to get a board by id.
     * @param id
     * @return
     */
    @GetMapping(Route.GET_BOARD_BY_ID)
    public BoardResponse getBoardById(
            @PathVariable("id") Long id) {
        return bingoService.getBoardById(id);
    }

    /**
     * Endpoint to evaluate which boards were the winner.
     * @param evaluateBoardRequest
     * @return  List of boards winner
     */
    @PostMapping(Route.EVALUATE)
    public Stream<BoardEntity> evaluateBoard(
            @RequestBody EvaluateBoardRequest evaluateBoardRequest){
        return bingoService.evaluateBoard(evaluateBoardRequest);
    }

    /**
     * Endpoint to get all figures.
     * @return list of figures
     */
    @GetMapping(Route.FIGURE)
    public List<FigureEntity> getAllFigure() {
        return bingoService.getAllFigure();
    }

    /**
     * Endpoint to update the positions of a figure entity.
     * @param figureRequest
     * @param request
     * @return Figure updated
     */
    @PutMapping(Route.FIGURE)
    public FigureEntity updatePositionWinnerFigure(@RequestBody @Valid UpdateFigureRequest figureRequest, HttpServletRequest request) {
        UserResponse user =  (UserResponse)request.getSession().getAttribute("user");
        return bingoService.updatePositionWinnerFigure(figureRequest, user);
    }
}

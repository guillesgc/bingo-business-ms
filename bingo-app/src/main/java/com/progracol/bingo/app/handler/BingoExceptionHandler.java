package com.progracol.bingo.app.handler;

import com.progracol.bingo.common.exception.BingoException;
import com.progracol.bingo.common.response.ErrorResponse;
import com.progracol.bingo.common.response.ExceptionErrorResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class BingoExceptionHandler {

    private Logger logger = LoggerFactory.getLogger(BingoExceptionHandler.class);

    /**
     * Handles BingoException.
     */
    @ExceptionHandler(BingoException.class)
    public ResponseEntity<ErrorResponse> validationErrorHandler(BingoException ex) {
        ExceptionErrorResponse exceptionErrorResponse = new ExceptionErrorResponse(ex.getErrorCode(), ex.getMessage());
        ErrorResponse errorResponse = new ErrorResponse(exceptionErrorResponse);

        logger.error("--BingoExceptionHandler:validationErrorHandler --code: [{}] --message: [{}]", ex.getErrorCode(), ex.getMessage());
        HttpStatus httpStatus = null;
        if (ex.getStatus() != null) {
            httpStatus = HttpStatus.resolve(ex.getStatus());
        }

        return new ResponseEntity(errorResponse, httpStatus != null ? httpStatus : HttpStatus.INTERNAL_SERVER_ERROR);
    }
}

package com.progracol.bingo.app.interceptor;

import com.progracol.bingo.common.exception.BingoException;
import com.progracol.bingo.common.response.UserResponse;
import com.progracol.bingo.core.service.AccessService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static com.progracol.bingo.common.constant.Header.AUTHORIZATION;

@Component
public class AccessTokenInterceptor implements HandlerInterceptor {

    private Logger logger = LoggerFactory.getLogger(AccessTokenInterceptor.class);

    @Autowired
    private AccessService accessService;

    /**
     * This method is going to check the authorization Bearer to allow access over the endpoints in this microservice
     * @param request
     * @param response
     * @param handler
     * @return authorized or unauthorized access
     */
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {
        String bearer = request.getHeader(AUTHORIZATION);
        if (bearer == null) {
            unauthorized();
        }
        UserResponse user = accessService.getUserFromToken(bearer);
        if (user == null) {
            unauthorized();
        }
        request.getSession().setAttribute("user", user);
        return true;
    }

    /**
     * Throw BingoException to unauthorized request petition
     */
    private void unauthorized() {
        throw new BingoException(HttpStatus.UNAUTHORIZED.value(),
                HttpStatus.UNAUTHORIZED.getReasonPhrase(),
                HttpStatus.UNAUTHORIZED.getReasonPhrase());
    }
}

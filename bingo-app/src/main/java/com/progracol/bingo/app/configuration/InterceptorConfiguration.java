package com.progracol.bingo.app.configuration;


import com.progracol.bingo.app.interceptor.AccessTokenInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import static com.progracol.bingo.common.constant.Header.ALL;
import static com.progracol.bingo.common.route.Route.HEALTH;

@Configuration
public class InterceptorConfiguration implements WebMvcConfigurer {

    @Autowired
    private AccessTokenInterceptor accessTokenInterceptor;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(accessTokenInterceptor).addPathPatterns(ALL)
                .excludePathPatterns(HEALTH);
    }
}
